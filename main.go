package main

import (
	"fmt"
	"log"
	"net/http"
  "os"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
      fmt.Fprintf(w, "hello world\n")
		default:
			http.NotFound(w, r)
		}
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	}
	addr := ":" + port

	log.Println("Listening on", addr)

	log.Fatal(http.ListenAndServe(addr, nil))
}

